import { useEffect, useState } from "react";
import {
    Text,
    View,
    ActivityIndicator,
    StyleSheet,
    Button,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
function Account() {
    const [currentAccountPK, setCurrentAccountPK] = useState();
    const [createAccountStatus, setCreateAccountStatus] = useState(false);
    return (
        <View style={styles.rootContainer}>
            <View style={styles.bodyContainer}>
                <View style={styles.headerContainer}>
                    <Text style={styles.headerTitle}>Sepolia Test Network</Text>
                    <Text style={styles.headerTagLine}>
                        V1: Programming for Blockchain II Currently you are using the
                        Ethereum test network
                    </Text>
                </View>
                <View style={styles.body}>
                    <View>
                        <Text style={styles.label}>Account Address:</Text>
                        <Text style={styles.value} selectable={true}>
                            *Display your Account Address*
                        </Text>
                    </View>
                    <View style={styles.prvateKeyContainer}>
                        <Text style={styles.label}>
                            Click the button below to get your private key
                        </Text>
                        <Text style={styles.value}>**Display your private key**</Text>
                        <Button style={styles.btn} title="Private Key" />
                    </View>
                    <View style={styles.prvateKeyContainer}>
                        <Text style={styles.label}>Create New Account</Text>
                        {!createAccountStatus ? (
                            <Ionicons
                                style={{ paddingTop: 10 }}
                                name="add-circle-sharp"
                                size={42}
                                color="green"
                            />
                        ) : (
                            <ActivityIndicator size="large" color="white" />
                        )}
                    </View>
                    <View style={styles.prvateKeyContainer}>
                        <Text style={styles.label}>Change the Account</Text>
                        <Text style={{ paddingTop: 5 }}></Text>
                        <Ionicons
                            name="ios-chevron-down-circle-sharp"
                            size={32}
                            color="white"
                        />
                    </View>
                    <View style={styles.footer}>
                        <Text style={styles.footerBalanceLoadingText}>
                            Loading the balance
                        </Text>
                    </View>
                </View>
            </View>
        </View>
    );
}
export default Account;
const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,
    },
    headerContainer: {
        alignItems: "center",
        backgroundColor: "#0a4f95",
        paddingVertical: 20,
    },
    headerTitle: {
        fontSize: 30,
        fontWeight: "bold",
        color: "white",
        paddingVertical: 20,
    },
    headerTagLine: {
        color: "white",
        fontSize: 17,
    },
    body: {
        flex: 1,
        justifyContent: "space-between",
    },
    label: {
        fontSize: 20,
        fontWeight: "bold",
        paddingVertical: 10,
        paddingLeft: 20,
    },
    value: {
        fontSize: 15,
        fontWeight: "bold",
        paddingVertical: 5,
        paddingLeft: 20,
    },
    bodyContainer: {
        flex: 1,
        justifyContent: "space-between",
    },
    footer: {
        backgroundColor: "#31689f",
        paddingVertical: 25,
        justifyContent: "center",
        alignItems: "center",
    },
    footerBalanceLoadingText: {
        color: "white",
        fontSize: 16,
    },
    prvateKeyContainer: {
        alignItems: "center",
        paddingLeft: 5,
        paddingRight: 10,
    },
});